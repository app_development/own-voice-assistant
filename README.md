# Own-Voice-Assistant

## Name
Individual voice assistant.

## Description
This python script acts as an personal assistent, where the voice and name of the assistant can be choosed by you, the user.

## Installation
- install python3
- install PyAudio - PortAudio, playsound, OpenAI, SpeechRec via pip or another packet manager.
- (only for windows: you can use the pyttsx3 but you need to install pyttsx3)

## Usage
Run the python script and call the assistant name you choosed. You should speak loud and clearly.
Running the python script via a shortcut makes it even faster and more suitable for everyday use.

## Authors and acknowledgment
AirFlow

## License
This is not an open-source project. By this in default - all rights are reserved -. Meaning that no one is allowed to make any copy/ sell/ modify/etc. of this Code. 


## Project status
Nearly finished, maybe an UI will follow.
