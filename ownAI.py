import speech_recognition as spr
import pyttsx3
import openai
from gtts import gTTS
from playsound import playsound

#---------------------------------------------------change---those-----------------------------------------------------------

openai.api_key = ""
ai_name = "John"
language = 'de'
isMac = True

#----------------------------------------------------------------------------------------------------------------------------


audioFileName = "audioFile.wav"

def generate_chatgpt_response(query):
    response = openai.Completion.create(engine= "text-davinci-003", prompt = query, max_tokens = 4000, n = 1, stop = None,)
    return response["choices"][0]["text"]

def audio_to_text(filename):
    recognizer = spr.Recognizer()
    with spr.AudioFile(filename) as source:
        audio = recognizer.record(source)
    try:
        return recognizer.recognize_google(audio_data=audio,language="de-DE")
    except:
        print("Unknown error in recognizing")

def speak_text(text):
    if(isMac):
        tts = gTTS(text=text,lang=language,slow=False)
        tts.save("sounds/tts.mp3")
        playsound("sounds/tts.mp3")
    else:
        engine = pyttsx3.init('sapi5')
        engine.say(text)
        engine.runAndWait()

def main():
    is_finished = False
    isFirstPass = True
    print("Start")
    while (not is_finished):
        print("")
        print("")
        with spr.Microphone() as mic:
            recognizer = spr.Recognizer()
            if not isFirstPass:
                print("Sage "+ai_name+" oder Ende")
                audio = recognizer.listen(source = mic)
            try:
                if not isFirstPass:
                    transcription = recognizer.recognize_google(audio_data= audio, language="de-DE")
                if(isFirstPass or transcription.lower() == ai_name.lower() or transcription.lower() == "jarves" or transcription.lower() == "jarvis" or transcription.lower() == "jaws"):
                    print("")
                    mic.pause_threshold = 0
                    playsound("sounds/activate.m4a")
                    audio = recognizer.listen(mic)
                    transcription = recognizer.recognize_google(audio_data= audio, language="de-DE")
                    # with open(audioFileName, "wb") as file:
                    #     file.write(audio.get_wav_data())

                    # transcription = audio_to_text(audioFileName)
                    if(transcription):
                        print("Query:   "+transcription)

                        print("")
                        response = generate_chatgpt_response(transcription)
                        print(ai_name+":  "+response)

                        speak_text(response)
                elif(transcription.lower() == "ende"):
                    speak_text("Ende")
                    print("Bye")
                    is_finished = True

            except Exception as e:
                print(e)       

            isFirstPass = False  

main()